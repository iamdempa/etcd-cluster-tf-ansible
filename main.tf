# Provider
provider "google" {
  # credentials = file("token.json")
  project     = var.project_name
  region      = var.region
  zone        = var.zone
}

# Configure the backend
terraform {
  backend "gcs" {
    bucket      = "tf_backend_gcp_banuka_jana_jayarathna_k8s"
    prefix      = "/etcd/terraform/state"
    # credentials = "token.json"
  }
}

# create vpc
resource "google_compute_network" "etcd-vpc" {
  name                    = "etcd-vpc"
  auto_create_subnetworks = "false"
}

# adding a firewall to the VPC
resource "google_compute_firewall" "etcd-ssh-all" {
  name    = "etcd-ssh-all"
  network = google_compute_network.etcd-vpc.name

  # ssh access, node_exporter (9100) and promethues (9090)
  allow {
    protocol = "tcp"
    ports    = ["22", "80", "9100", "9090"]
  }

  source_ranges = ["0.0.0.0/0"]
  # source_ranges = ["${google_compute_instance.kube-master.network_interface.0.access_config.0.nat_ip}", "${google_compute_instance.kube-minion[0].network_interface.0.access_config.0.nat_ip}", "${google_compute_instance.kube-minion[1].network_interface.0.access_config.0.nat_ip}", "${google_compute_instance.nfs-server.network_interface.0.access_config.0.nat_ip}"]
  target_tags = ["first-two-servers", "last-two-servers"]
}



# create subnet for two slaves
resource "google_compute_subnetwork" "first-two-servers-sub" {
  name          = "first-two-servers-sub"
  ip_cidr_range = var.master_cidr
  region        = var.master_subnet_region
  network       = google_compute_network.etcd-vpc.name
  depends_on    = [google_compute_network.etcd-vpc]
  private_ip_google_access = "false"
}

# create subnet for other two slaves
resource "google_compute_subnetwork" "last-two-servers-sub" {
  name          = "last-two-servers-sub"
  ip_cidr_range = var.minion_cidr
  region        = var.minion_subnet_region
  network       = google_compute_network.etcd-vpc.name
  depends_on    = [google_compute_network.etcd-vpc]
  private_ip_google_access = "true"
}


resource "google_compute_instance" "first-two-servers" {
  count   = var.server_count  
  name         = "first-two-servers-${count.index}"
  machine_type = var.machine_type
  zone         = var.master_zone

  tags = ["first-two-servers"]

  boot_disk {
    initialize_params {
      image = var.machine_image
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.first-two-servers-sub.name

    access_config {
      // Ephemeral IP
    }
  }

  metadata = {
    Name = "first-two"
  }

  metadata_startup_script = <<-EOF
              #!/bin/bash    
              sudo apt-get update -y
              sudo apt-get install ansible -y  
              sudo apt install python -y
              sudo echo 'ok' > /root/hi.txt
              sudo mkdir -p /root/.ssh/ && touch /root/.ssh/authorized_keys
              sudo echo "${file("/root/.ssh/id_rsa.pub")}" >> /root/.ssh/authorized_keys  
            EOF
  # metadata_startup_script = "echo hi > /test.txt"
}



resource "google_compute_instance" "last-two-servers" {
  count   = var.server_count  
  name         = "last-two-servers-${count.index}"  
  machine_type = var.machine_type
  zone         = var.master_zone

  tags = ["last-two-servers"]

  boot_disk {
    initialize_params {
      image = var.machine_image
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.last-two-servers-sub.name

    access_config {
      // Ephemeral IP
    }
  }

  metadata = {
    Name = "last-two"
  }

  metadata_startup_script = <<-EOF
              #!/bin/bash    
              sudo apt-get update -y
              sudo apt-get install ansible -y  
              sudo apt install python -y
              sudo echo 'ok' > /root/hi.txt
              sudo mkdir -p /root/.ssh/ && touch /root/.ssh/authorized_keys
              sudo echo "${file("/root/.ssh/id_rsa.pub")}" >> /root/.ssh/authorized_keys  
            EOF
  # metadata_startup_script = "echo hi > /test.txt"
}


resource "null_resource" "web3" {

 triggers  = {
    key = "${uuid()}"
  }

  provisioner "local-exec" {
      command = "rm -rf ~/.ssh/known_hosts"
  }

  provisioner "local-exec" {
        command = <<EOD
cat <<EOF > /etc/ansible/hosts
[all_servers] 
${google_compute_instance.first-two-servers[0].network_interface.0.access_config.0.nat_ip}
${google_compute_instance.first-two-servers[1].network_interface.0.access_config.0.nat_ip}
${google_compute_instance.last-two-servers[0].network_interface.0.access_config.0.nat_ip}
${google_compute_instance.last-two-servers[1].network_interface.0.access_config.0.nat_ip}
EOF
EOD
    }
}