systemctl stop etcd

rm -rf /etc/etcd.conf && rm -rf /etc/systemd/system/etcd.service && rm -rf /usr/local/bin/etcd && yum install wget -y

curl -s https://api.github.com/repos/etcd-io/etcd/releases/latest \
  | grep browser_download_url \
  | grep linux-amd64 \
  | cut -d '"' -f 4 \
  | wget -qi -

tar xvf *.tar.gz
cd etcd-*/
sudo mv etcd* /usr/local/bin/
cd ~
rm -rf /tmp/etcd  

sleep 2

INTERNAL_IP=$(ip -o route get to 8.8.8.8 | sed -n 's/.*src \([0-9.]\+\).*/\1/p')


cat << EOF > /etc/etcd.conf
#SELF_IP is the IP of the node where this file resides.
SELF_IP=$INTERNAL_IP
#IP of Node 1
NODE_1_IP=10.128.15.10
#IP of Node 2
NODE_2_IP=10.128.15.11
#IP of Node 3
NODE_3_IP=10.128.15.12
EOF


SELF_IP=$INTERNAL_IP
NODE_1_IP=10.128.15.10
NODE_2_IP=10.128.15.11
NODE_3_IP=10.128.15.12


SELF_NAME=$(hostname)
NODE_1_NAME=etcd-0
NODE_2_NAME=etcd-1
NODE_3_NAME=etcd-2

sleep 2


cat << EOF > /etc/systemd/system/etcd.service
[Unit]
Description=etcd
Documentation=http://github.com/coreos

[Service]
Type=notify
EnvironmentFile=/etc/etcd.conf
ExecStart=/usr/local/bin/etcd \
 --name ${SELF_NAME} \
 --initial-advertise-peer-urls http://${SELF_IP}:2380 \
 --listen-peer-urls http://${SELF_IP}:2380 \
 --listen-client-urls http://${SELF_IP}:2379,http://127.0.0.1:2379 \
 --advertise-client-urls http://${SELF_IP}:2379 \
 --initial-cluster-token cluster-0 \
 --initial-cluster ${NODE_1_NAME}=http://${NODE_1_IP}:2380,${NODE_2_NAME}=http://${NODE_2_IP}:2380,${NODE_3_NAME}=http://${NODE_3_IP}:2380 \
 --initial-cluster-state new \
 --data-dir=/var/lib/etcd
RestartSec=5
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=etcd
Restart=always
User=root
Group=root

ExecStop=/bin/kill -s TERM $MAINPID

[Install]
WantedBy=multi-user.target
EOF

sleep 5

echo "daemon-reload"

systemctl daemon-reload

sleep 5

echo "enable etcd"

systemctl enable etcd

sleep 7

echo "start etcd"

sleep 3

systemctl start etcd

sleep 3

echo "get cluster health"

etcdctl cluster-health

echo "done"