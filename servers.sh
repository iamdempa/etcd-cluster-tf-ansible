for i in 0 1 2; do
  gcloud beta compute --project=possible-origin-236919 instances create etcd-${i} \
  --zone=us-central1-a --machine-type=e2-medium --private-network-ip 10.128.15.1${i}  \
  --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE \
  --service-account=309640573903-compute@developer.gserviceaccount.com \
  --scopes=https://www.googleapis.com/auth/cloud-platform --image=centos-7-v20201112 \
  --image-project=centos-cloud --boot-disk-size=20GB --boot-disk-type=pd-standard \
  --boot-disk-device-name=etcd-${i} --no-shielded-secure-boot --shielded-vtpm \
  --shielded-integrity-monitoring --reservation-affinity=any
done

for i in 0 1 2; do
    gcloud --format="value(networkInterfaces[0].networkIP)" compute instances list --filter="name=('etcd-${i}')"
done